import '../../../scss/layouts/front-page/front-page.scss';
import { Carousel } from "../../components/carousel";

class FrontPage {

    private carousel: Carousel;

    constructor() {
        this.carousel = new Carousel();
    }

}

new FrontPage();
