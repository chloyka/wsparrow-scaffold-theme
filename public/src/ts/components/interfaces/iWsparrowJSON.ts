export interface WSparrowJSON {
    response: JSON
    success: boolean,
    html: string
}

