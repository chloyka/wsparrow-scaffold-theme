import { WSparrowJSON } from "./interfaces/iWsparrowJSON";

export class Carousel {

    url = '/wp-json/wsparrow/get-carousel';

    message: object;

    constructor() {
        document.addEventListener('readystatechange', () => {
            if (document.readyState === 'complete') {
                var element = <HTMLElement>document.querySelector('[data-post-id]');

                this.message = {
                    "post_id": element.dataset.postId
                };

                this.getCarousel();
            }
        })
    }

    private getCarousel = () => {
        try {
            this.request().then((response: WSparrowJSON) => {
                if (false !== response.success) {
                    let html = <HTMLElement>document.querySelector('.carousel');
                    html.innerHTML = response.html;
                }
            });
        } catch (e) {
            console.log(e);
        }

    };

    private request = () => new Promise((resolve, reject) => {
        var request = new XMLHttpRequest;
        request.open('POST', this.url, true);
        request.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
        request.onload = () => {
            if (request.status >= 200 && request.status < 300) {
                resolve(JSON.parse(request.response));
            } else {
                reject({
                    status: request.status,
                    statusText: request.statusText
                });
            }
        };
        request.onerror = () => {
            reject({
                status: request.status,
                statusText: request.statusText
            });
        };

        request.send(JSON.stringify(this.message));
    });
}
