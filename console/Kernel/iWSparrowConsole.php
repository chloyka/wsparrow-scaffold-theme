<?php

namespace WSparrow\Console\Kernel;

defined('WSPARROW_DO_CLI') || die('hard');

interface iWSparrowConsole
{

    /**
     * Парсим переданные аргументы
     *
     * @return void
     * @author Chloyka
     * @since 1.0
     */
    public function parse(): void;

    /**
     * Функция завершения работы скрипта
     *
     * @author Chloyka
     * @since 1.0
     */
    public function terminate(): void;

    /**
     * Геттер
     *
     * @param string $property
     * @return mixed|void
     * @author Chloyka
     * @since 1.0
     */
    public function __get(string $property);

}
