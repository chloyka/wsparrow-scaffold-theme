<?php

namespace WSparrow\Console\Kernel;

use WSparrow\Console\Commands\{ConsoleClear, ConsoleHelp, Make\ConsoleMakeCpt};

defined('WSPARROW_DO_CLI') || die('hard');


/**
 * Class WSparrowCLI
 * @package WSparrow\CLI
 * @author Chloyka
 * @since 1.0
 */
class WSparrowConsole implements iWSparrowConsole
{

    /**
     * Содержит инстанс класса
     *
     * @var null|void|self
     * @author Chloyka
     * @since 1.0
     */
    protected static $_instance = null;


    /**
     * Статус выполнения задачи
     *
     * @var string
     * @author Chloyka
     * @since 1.0
     */
    protected $status;

    /**
     * Лист ожидаемых комманд
     *
     * @var array
     */
    protected $commands = array(
        "help",
        "clear",
        "make:",
        "name:"
    );


    /**
     * WSparrowConsole constructor.
     */
    private function __construct()
    {
        $this->status = 'Успешно!';
    }

    /**
     * Парсим переданные аргументы
     *
     * @return void
     * @author Chloyka
     * @since 1.0
     */
    public function parse(): void
    {
        $args = getopt("", $this->commands);
        $response = null;
        if (isset($args["help"])) {
            $this->getResponse(
                ConsoleHelp::instance()
            );
        } elseif (isset($args["clear"])) {
            $this->getResponse(
                ConsoleClear::instance()
            );
        } elseif (isset($args["make"])) {
            switch ($args["make"]){
                case 'cpt':
                    $this->getResponse(
                        ConsoleMakeCpt::instance()
                    );
                    break;
                case 'migration':
                    echo $args["make"] . '';
                    break;
                default:
                    $this->getResponse(
                        ConsoleHelp::instance()
                    );
                    break;
            }
        } else {
            $this->getResponse(
                ConsoleHelp::instance()
            );
        }

    }

    /**
     * Получаем ответ от модуля комманды и умираем
     *
     * @param object $class
     * @return void;
     */
    public function getResponse($class): void
    {
        die($class->response());
    }

    /**
     * Функция завершения работы скрипта
     *
     * @author Chloyka
     * @since 1.0
     */
    public function terminate(): void
    {
        $was_busy = round((float)microtime(true) - (float)WSPARROW_DO_CLI, 5);
        die($this->status . ' Время выполнения скрипта ' . $was_busy . ' сек');
    }


    /**
     * Геттер
     *
     * @param string $property
     * @return mixed|void
     * @author Chloyka
     * @since 1.0
     */
    public function __get(string $property)
    {
        if (property_exists($this, $property)) {
            return $this->$property;
        }
    }

    /**
     * Возвращает текущий инстанс класса self
     *
     * @return WSparrowConsole
     * @author Chloyka
     * @since 1.0
     */
    public static function newSelf(): WSparrowConsole
    {
        return new self();
    }

    /**
     * Возвращает текущий экземпляр объекта
     *
     * @return void|self|null
     * @author Chloyka
     * @since 1.0
     */
    public static function instance()
    {
        {
            if (isset($_POST['action']) && $_POST['action'] === 'heartbeat') {
                return;
            }

            if (is_null(self::$_instance)) {
                self::$_instance = self::newSelf();
            }

            return self::$_instance;
        }
    }
}
