<?php

namespace WSparrow\Console\Commands;

defined('WSPARROW_DO_CLI') || die('hard');

/**
 * Команда --help
 *
 * Выводит в консоль страницу помощи
 *
 * @package WSparrow\Console\Commands
 * @author Chloyka
 * @since 1.0
 */
class ConsoleHelp extends WSparrowConsoleCommand
{

    protected static $_instance = null;

    /**
     * ConsoleHelp constructor.
     */
    private function __construct(){
        $this->make();
        $this->response();
    }


    /**
     * Функция возврата ответа от сервера
     *
     * @return string
     * @author Chloyka
     * @since 1.0
     */
    public function response(): string
    {
        $response = "/*" . PHP_EOL;
        $response .= "|--------------------------------------------------------------------------" . PHP_EOL;
        $response .= "| WSparrow CLI HELP" . PHP_EOL;
        $response .= "|--------------------------------------------------------------------------" . PHP_EOL;
        $response .= "|" . PHP_EOL;
        $response .= "| Для очистки консоли добавьте флаг --clear" . PHP_EOL;
        $response .= "|" . PHP_EOL;
        $response .= "| Для создания миграции добавьте флаг --make migration --name {migration_name}" . PHP_EOL;
        $response .= "|" . PHP_EOL;
        $response .= "| Для создания Custom Post Type добавьте флаг --make cpt --name {cpt_slug} " . PHP_EOL;
        $response .= "|" . PHP_EOL;
        $response .= "*/" . PHP_EOL;
        return $response;
    }

}
