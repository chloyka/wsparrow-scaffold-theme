<?php

namespace WSparrow\Console\Commands;

defined('WSPARROW_DO_CLI') || die('hard');

/**
 * Интерфейс создания консольной комманды
 *
 * @package WSparrow\Console\Commands
 * @author Chloyka
 * @since 1.0
 */
interface iWSparrowConsoleCommand{

    /**
     * Функция создания объекта
     *
     * @return void
     * @author Chloyka
     * @since 1.0
     */
    public function make(): void;

    /**
     * Функция возврата ответа от сервера
     *
     * @return string
     * @author Chloyka
     * @since 1.0
     */
    public function response(): string;

}
