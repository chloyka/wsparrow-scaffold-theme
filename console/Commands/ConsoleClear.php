<?php

namespace WSparrow\Console\Commands;

defined('WSPARROW_DO_CLI') || die('hard');

/**
 * Команда --clear
 *
 * Очищает консоль (Очень полезно при разработке)
 *
 * Останется ли функция при выходе в продакшн?
 *
 * Нужно подумать.
 *
 * @package WSparrow\Console\Commands
 * @author Chloyka
 * @since 1.0
 */
class ConsoleClear extends WSparrowConsoleCommand
{


    /**
     * ConsoleHelp constructor.
     */
    private function __construct()
    {
        $this->make();
    }

    /**
     * Функция создания объекта
     *
     * @author Chloyka
     * @since 1.0
     */
    public function make(): void
    {
        if ($this->isWindows()) {
            $iteration = 0;
            do {
                echo chr(0x0C);
                $iteration++;
            } while ($iteration < 300);
        } else {
            system('clear');
        }

    }

    /**
     * Проверяем, является ли текущая система Windows подобной
     *
     * @return bool
     * @author Chloyka
     * @since 1.0
     */
    private function isWindows(){
        return strtoupper(substr(PHP_OS, 0, 3)) === 'WIN';
    }

    /**
     * Функция возврата ответа от сервера
     *
     * @return string
     * @author Chloyka
     * @since 1.0
     */
    public function response(): string
    {
        $response = "/*" . PHP_EOL;
        $response .= "|--------------------------------------------------------------------------" . PHP_EOL;
        $response .= "| Консоль успешно очищена" . PHP_EOL;
        $response .= "|--------------------------------------------------------------------------" . PHP_EOL;
        $response .= "*/" . PHP_EOL;
        return $response;
    }

}
