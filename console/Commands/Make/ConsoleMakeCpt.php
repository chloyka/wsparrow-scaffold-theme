<?php

namespace WSparrow\Console\Commands\Make;

use WSparrow\Console\Commands\WSparrowConsoleCommand;

defined('WSPARROW_DO_CLI') || die('hard');

/**
 * Команда --make cpt
 *
 * Создает Custom Post Type
 *
 * @package WSparrow\Console\Commands\Make
 * @author Chloyka
 * @since 1.0
 */
class ConsoleMakeCpt extends WSparrowConsoleCommand
{


    /**
     * ConsoleHelp constructor.
     */
    private function __construct(){
        $this->make();
        $this->response();
    }

    /**
     * Функция создания объекта
     *
     * @author Chloyka
     * @since 1.0
     */
    public function make(): void
    {
       $template_rows = $this->getConfigTemplateFile('cpt');
       if(!is_null($template_rows)){
           $template_rows['slug'] = readline('Введите слаг CPT: ' . PHP_EOL);
           foreach ($template_rows['plurals'] as $key => $value){
               $template_rows['plurals'][$key] = readline('Введите ' . $key . ': ' . PHP_EOL);
           }
           foreach ($template_rows['settings'] as $key => $value){
               if($key == 'labels'){
                   foreach ($template_rows['settings']['labels'] as $key => $value){
                       if($value == 'string'){
                           $template_rows['settings']['labels'][$key] = readline('Введите ' . $key . ': ' . PHP_EOL);
                       } elseif($key == 'add_new'){
                           $template_rows['settings']['labels'][$key] .= $template_rows['plurals']['single'];
                       } elseif($key == 'add_new_item') {
                           $template_rows['settings']['labels'][$key] .= $template_rows['plurals']['single'];
                       } elseif($key == 'edit_item') {
                           $template_rows['settings']['labels'][$key] .= $template_rows['plurals']['single'];
                       } elseif($key == 'new_item') {
                           $template_rows['settings']['labels'][$key] .= $template_rows['plurals']['single'];
                       } elseif($key == 'view_item') {
                           $template_rows['settings']['labels'][$key] .= $template_rows['plurals']['single'];
                       } elseif($key == 'search_items') {
                           $template_rows['settings']['labels'][$key] .= $template_rows['plurals']['find'];
                       } elseif($key == 'not_found') {
                           $template_rows['settings']['labels'][$key] .= $template_rows['plurals']['no'];
                       } elseif($key == 'not_found_in_trash') {
                           $template_rows['settings']['labels'][$key] .= $template_rows['plurals']['multiple'];
                       }

                   }
               } elseif($value == 'string'){
                   $template_rows['settings'][$key] = readline('Введите ' . $key . ': ' . PHP_EOL);
               } elseif($value == 'bool') {
                   $template_rows['settings'][$key] = (bool)readline('Укажите true, если да, false, если нет (boolean). Укажите ' . $key . ': ' . PHP_EOL);
               } elseif($value == 'array') {
                   $template_rows['settings'][$key] = explode(', ', readline('Перечислите все необходимые значения, разделяя их запятыми и пробелами. (1, 2, 3, 4, ... ,etc) Укажите ' . $key . ': ' . PHP_EOL));
               }
           }
           $this->makeConfigFile($template_rows);
       }
    }

    /**
     * Функция возврата ответа от сервера
     *
     * @return string
     * @author Chloyka
     * @since 1.0
     */
    public function response(): string
    {
        $response = "/*" . PHP_EOL;
        $response .= "|--------------------------------------------------------------------------" . PHP_EOL;
        $response .= "| Тип записи успешно создан" . PHP_EOL;
        $response .= "|--------------------------------------------------------------------------" . PHP_EOL;
        return $response;
    }

    /**
     * Возвращает текущий инстанс класса ConsoleHelp
     *
     * @return ConsoleMakeCpt
     * @author Chloyka
     * @since 1.0
     */
    protected static function newSelf(): ConsoleMakeCpt
    {
        return new self();
    }

    /**
     * Возвращает текущий экземпляр объекта
     *
     * @return void|ConsoleMakeCpt|null
     * @author Chloyka
     * @since 1.0
     */
    public static function instance()
    {

        if (isset($_POST['action']) && $_POST['action'] === 'heartbeat') {
            return;
        }

        if (is_null(self::$_instance)) {
            self::$_instance = self::newSelf();
        }

        return self::$_instance;
    }

}
