<?php

namespace WSparrow\Console\Commands;

defined('WSPARROW_DO_CLI') || die('hard');

/**
 * Абстрактный класс консольной комманды
 *
 * Используется в качестве шаблона для создания комманды
 *
 * @package WSparrow\Console\Commands
 * @author Chloyka
 * @since 1.0
 */
abstract class WSparrowConsoleCommand implements iWSparrowConsoleCommand
{
    /**
     * Содержит инстанс класса
     *
     * @var null|void|static
     * @author Chloyka
     * @since 1.0
     */
    protected static $_instance = null;

    /**
     * Функция создания объекта
     *
     * @author Chloyka
     * @since 1.0
     */
    public function make(): void
    {
        // Nothing to do;
    }

    /**
     * Возвращает текущий экземпляр объекта
     *
     * @return void|static|null
     * @author Chloyka
     * @since 1.0
     */
    public static function instance()
    {

        if (isset($_POST['action']) && $_POST['action'] === 'heartbeat') {
            return;
        }

        if (is_null(self::$_instance)) {
            self::$_instance = new static();
        }

        return self::$_instance;

    }

    /**
     * Возврашает шаблон объекта в виде массива
     *
     * @param string $file
     * @return array|void
     * @author Chloyka
     * @since 1.0
     */
    protected function getConfigTemplateFile(string $file)
    {
        if (defined('WSPARROW_CONFIG_PATH')) {
            $file_path = WSPARROW_CONFIG_PATH . 'example-templates/' .$file . '-example.json';
            if(file_exists($file_path)){
                var_dump($file_path);
                return json_decode(file_get_contents($file_path), true);
            }
        }
    }

    /**
     * Собирает параметры в JSON файл
     *
     * Данные JSON файлы используются темой для генерации необходимых объектов
     *
     * @param array $props
     * @return void
     * @author Chloyka
     * @since 1.0
     */
    protected function makeConfigFile(array $props): void
    {
        if (defined('WSPARROW_CONFIG_PATH')) {
            file_put_contents(
                WSPARROW_CONFIG_PATH . $props['template-type'] . '/' . $props['template-type'] . '-' . $props['slug'] . '.json',
                json_encode($props, JSON_UNESCAPED_UNICODE)
            );
        }
    }

}
