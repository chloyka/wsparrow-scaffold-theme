<?php

namespace WSparrow\Console\Helpers;

defined('WSPARROW_DO_CLI') || die('hard');

/**
 * Вспомогательные функции
 *
 * Класс может содержать только статические функции
 *
 * @package WSparrow\Console\Helpers
 * @author Chloyka
 * @since 1.0
 */
final class ConsoleHelpers
{

}
