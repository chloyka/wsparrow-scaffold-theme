<?php


namespace WSparrow\Modules\CustomFields;

use Carbon_Fields\{Carbon_Fields, Container, Field};
use WSparrow\{Helpers\WSparrowHelpers, WSparrowController};

defined('ABSPATH') || die('hard');

class WSparrowCustomFields extends WSparrowController
{
    /**
     * Содержит инстанс класса
     *
     * @var null|void|static
     * @author Chloyka
     * @since 1.0
     */
    protected static $_instance = null;

    /**
     * Метод инициализации класса
     *
     * Делаем все важные штуки тут
     *
     * @return void
     * @author Chloyka
     * @since 1.0
     */
    public function onInit(): void
    {
        Carbon_Fields::boot();
        add_action( 'carbon_fields_register_fields', array($this, 'getCustomFieldsPresets') );
    }

    /**
     * Получаем информацию о регистрируемых поляв в виде массивов
     *
     * Отдаем каждый массив на регистрацию
     *
     * @return void
     * @author Chloyka
     * @since 1.0
     */
    public function getCustomFieldsPresets(): void
    {
        $presets = WSparrowHelpers::getLocalJsonFileNames('custom-fields');
        foreach ($presets as $preset){
            $fields = WSparrowHelpers::getLocalJson($preset);
            $this->registerCustomFields($fields);
        }
    }

    private function setFieldsLayout($field, $field_name){

        $current_field = Field::make($field['type'], $field_name, $field['name']);

        switch ($field['type']):
            case 'association':
                $current_field->set_types($field['associated_with']);
                $current_field->set_max($field['max']);
                $field_list = $current_field;
                break;
            case 'select':
            case 'multiselect':
                $current_field->add_options($field['options']);
                $field_list = $current_field;
                break;
            case 'date':
            case 'time':
                $current_field->set_storage_format($field['output']);
                $current_field->set_input_format($field['input'], $field['input']);
                $field_list = $current_field;
                break;
            case 'complex':
                $complex_fields = array();
                foreach ($field['fields'] as $complex_key => $complex_value){
                    $complex_fields[] = $this->setFieldsLayout($complex_value, $complex_key);
                }
                $current_field->add_fields($complex_fields);
                $current_field->set_layout($field['layout']);
                $current_field->set_header_template($field['header_template']);
                $field_list = $current_field;
                break;
            case 'image':
                $current_field->set_value_type($field['return']);
                $field_list = $current_field;
                break;
            default:
                $field_list = $current_field;
                break;
        endswitch;
        return $field_list;
    }

    /**
     * Регистрируем кастомные поля
     *
     * @param array $fields
     * @return void
     * @author Chloyka
     * @since 1.0
     */
    private function registerCustomFields(array $fields): void
    {
        $post_type = $fields['post_type'];
        $container_name = $fields['container_name'];
        $tabs = $fields['tabs'];
        $i = 0;

        $container = Container::make("post_meta", "{$post_type}", $container_name)
           ->where('post_type', '=', $post_type);

        foreach ($tabs as $tab_name => $field_set){
            $field_list = null;

            foreach ($field_set as $field_name => $field) {
                $field_list[] = $this->setFieldsLayout($field, $field_name);
            }

            $container->add_tab($tab_name, $field_list);

        }
    }

}
