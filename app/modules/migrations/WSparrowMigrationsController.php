<?php

namespace WSparrow\Modules\Migrations;

use wpdb;
use WSparrow\Helpers\WSparrowHelpers;
use WSparrow\WSparrowController;

defined('ABSPATH') || die('hard');

/**
 * Class WSparrowMigrationsController
 *
 * Главный абстрактный класс миграций приложения
 *
 * @package WSparrow\Modules\Migrations
 * @author Chloyka
 * @since 1.0
 */
abstract class WSparrowMigrationsController extends WSparrowController
{
    /**
     * Содержит инстанс класса
     *
     * @var null|void|static
     * @author Chloyka
     * @since 1.0
     */
    protected static $_instance;

    /**
     * Содержит wpdb
     *
     * @var wpdb
     */
    protected $wpdb;

    /**
     * Содержит шаблоны миграций
     *
     * @var array
     */
    protected $templates;

    /**
     * Метод инициализации класса
     *
     * Делаем все важные штуки тут
     *
     * @return void
     * @author Chloyka
     * @since 1.0
     */
    public function onInit(): void
    {
        global $wpdb;
        $this->wpdb = $wpdb;
        $this->makeMigrations();
    }

    /**
     * Создает миграции в базе данных
     *
     * @return void
     * @author Chloyka
     * @since 1.0
     */
    abstract protected function makeMigrations(): void;


    /**
     * Удяляем все не заявленные в текущей структуре миграции поля
     *
     * @param array $args
     * @param array $custom_fields
     * @param string|null $table_name
     * @return void
     * @author Chloyka
     * @since 1.0
     */
    private function deleteUnusedColumns(array $args, array $custom_fields, string $table_name = null): void
    {
        if(is_null($table_name)){
            $table_name = $this->wpdb->get_blog_prefix() . $args['table_name'];
        }

        $columns = $this->wpdb->get_results("SHOW COLUMNS FROM {$table_name};", ARRAY_A);

        $fields = array();

        foreach ($columns as $column){
            $fields[$column['Field']] = $column['Field'];
        }

        unset($fields['post_id']);
        unset($fields['ID']);
        foreach ($args as $key => $arg) {
            if ($key === 'table_name') continue;
            unset($fields[$key]);
        }

        foreach ($custom_fields as $tab => $field_set){
            foreach ($field_set as $field_name => $value){
                unset($fields[$field_name]);
            }
        }

        foreach ($fields as $key => $field){
            $this->wpdb->query("ALTER TABLE `{$table_name}` DROP `{$key}`;");
        }
    }

    /**
     * Функция создания таблиц
     *
     * @param array $args
     * @return void
     * @author Chloyka
     * @since 1.0
     */
    public function createTable(array $args): void
    {

        if(!empty($args['table_name'])){
            require_once ABSPATH . 'wp-admin/includes/upgrade.php';

            $table_name = $this->wpdb->get_blog_prefix() . "wsparrow_{$args['table_name']}";

            $custom_fields = WSparrowHelpers::getLocalJson(WSPARROW_CONFIG_PATH . "custom-fields/custom-fields-{$args['table_name']}.json")['tabs'];

            $sql = "CREATE TABLE {$table_name} (" . PHP_EOL;
            $sql .= "ID bigint(20) unsigned NOT NULL auto_increment, " . PHP_EOL;
            $sql .= "post_id VARCHAR(25) NOT NULL default '', " . PHP_EOL;
            foreach ($args as $key => $arg) {
                if ($key === 'table_name') continue;
                $sql .= "{$key} {$arg['type']} {$arg['is_null']} default {$arg['default']}, " . PHP_EOL;
            }

            foreach ($custom_fields as $tab => $field_set){
                foreach ($field_set as $field_name => $value){
                    $sql .= "{$field_name} LONGTEXT NOT NULL default '', " . PHP_EOL;
                }
            }

            $sql .= "PRIMARY KEY  (ID)" . PHP_EOL;
            $sql .= ")" . PHP_EOL;
            $sql .= "DEFAULT CHARACTER SET {$this->wpdb->charset} COLLATE {$this->wpdb->collate};" . PHP_EOL;
            dbDelta($sql);
            $this->deleteUnusedColumns($args, $custom_fields, $table_name);
        }
    }

}
