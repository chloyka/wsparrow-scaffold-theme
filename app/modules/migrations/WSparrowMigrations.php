<?php

namespace WSparrow\Modules\Migrations;

use WSparrow\Helpers\WSparrowHelpers;

defined('ABSPATH') || die('hard');

/**
 * Класс управления миграциями
 *
 * @package WSparrow\Modules\Migrations
 * @author Chloyka
 * @since 1.0
 */
class WSparrowMigrations extends WSparrowMigrationsController
{
    /**
     * Содержит инстанс класса
     *
     * @var null|void|static
     * @author Chloyka
     * @since 1.0
     */
    protected static $_instance = null;

    /**
     * Содержит ссылки на файлы миграций
     *
     * @var array
     */
    private $migration_files = array();


    /**
     * Функция создания миграций
     *
     * @author Chloyka
     * @since 1.0
     */
    protected function makeMigrations(): void
    {

        if(is_admin()){
            $last_updated = WSparrowHelpers::getLocalJson(WSPARROW_CONFIG_PATH . 'last_updated/migrations-current-site-version.json');
            $last_updated_custom_fields = WSparrowHelpers::getLocalJson(WSPARROW_CONFIG_PATH . 'last_updated/custom-fields-current-site-version.json');

            foreach ($last_updated as $path => $time) {
                $table_name = WSparrowHelpers::getLocalJson($path)['table_name'];
                $is_exists = (bool)WSparrowHelpers::testQuery($this->wpdb, $table_name);
                $is_need_to_update = false;

                foreach ($last_updated_custom_fields as $field_path => $field_time){
                    $field_post_type = WSparrowHelpers::getLocalJson($field_path)['post_type'];
                    if(filemtime($field_path) !== $field_time && $field_post_type == $table_name){
                        $is_need_to_update = true;
                    }
                }

                if (false == $is_exists || filemtime($path) !== $time || true == $is_need_to_update) {
                    $this->migration_files[] = $path;
                }
            }

            foreach ($this->migration_files as $file){
                $this->createTable(WSparrowHelpers::getLocalJson($file));
            }

            WSparrowHelpers::makeFilesLastUpdateInfo(array('migrations'), true);
            WSparrowHelpers::makeFilesLastUpdateInfo(array('custom-fields'), true);
        }
    }

}
