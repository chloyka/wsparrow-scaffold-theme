<?php

namespace WSparrow\Modules\Rest;

use WSparrow\Modules\Routing\WSparrowRouting;
use WSparrow\WSparrowController;

defined('ABSPATH') || die('hard');

/**
 * Отвечает за Rest API слайдера предыдущих просмотров
 *
 * @package WSparrow\Modules\Rest
 * @author Chloyka
 * @since 1.0
 */
class WSparrowCarouselRest extends WSparrowController
{

    /**
     * Содержит инстанс класса
     *
     * @var null|void|static
     * @author Chloyka
     * @since 1.0
     */
    protected static $_instance = null;

    /**
     * Метод инициализации класса
     *
     * Делаем все важные штуки тут
     *
     * @return void
     * @author Chloyka
     * @since 1.0
     */
    public function onInit(): void
    {
        if (!isset($_SESSION)) {
            session_start();
        }

        add_action('rest_api_init', array($this, 'registerRestAddress'));
    }

    /**
     * Регистрируем REST API точку
     *
     * @return void
     * @author Chloyka
     * @since 1.0
     */
    public function registerRestAddress(): void
    {
        register_rest_route('wsparrow', 'get-carousel', array(
            'methods' => 'POST',
            'callback' => array($this, 'endpointMethod'),
            'permission_callback' => null,
            'args' => array(),
        ));
    }

    /**
     * Парсим JSON из input
     *
     * @return array
     * @author Chloyka
     * @since 1.0
     */
    private function getParsedRequest(): array
    {
        return json_decode(file_get_contents('php://input'), true);
    }

    /**
     * REST API эндпоинт
     *
     * @return void
     * @author Chloyka
     * @since 1.0
     */
    public function endpointMethod()
    {
        $request = $this->getParsedRequest();

        if (!empty($request['post_id'])) {
            $post = get_post($request['post_id']);
        } else {
            wp_send_json(array(
                'success' => false,
                'message' => 'No post ID'
            ), 200);
            exit;
        }

        if ($post->post_type == 'artists' || $post->post_type == 'albums') {

            $carousel = array();

            if (isset($_SESSION['carousel'])) {
                $carousel = maybe_unserialize($_SESSION['carousel']);
            }
            $carousel_to_render = null;
            $carousel[$post->post_type][$post->ID] = false;

            foreach ($carousel as $cpt => $items) {
                foreach ($items as $ID => $is_show) {
                    if ($ID == $post->ID) {
                        $carousel[$cpt][$ID] = false;
                    } else {
                        global $wpdb;
                        $carousel[$cpt][$ID] = true;
                        if ($cpt == $post->post_type) {
                            $carousel_to_render['carousel'][$ID]['url'] = get_permalink($ID);
                            $carousel_to_render['carousel'][$ID]['title'] = get_the_title($ID);
                            $carousel_to_render['carousel'][$ID]['image'] = $wpdb->get_results("SELECT album_image FROM wp_wsparrow_{$cpt} WHERE `post_id` = {$ID}", ARRAY_A)[0]['album_image'];
                        }
                    }
                }
            }

            $_SESSION['carousel'] = serialize($carousel);

            $render = WSparrowRouting::instance();
            ob_start();
            $render->render($carousel_to_render, 'carousel');

            wp_send_json(array(
                'html' => ob_get_clean(),
                'success' => true
            ), 200);
        } else {
            wp_send_json(array(
                'success' => false
            ), 200);
        }
    }

}
