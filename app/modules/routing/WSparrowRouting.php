<?php


namespace WSparrow\Modules\Routing;


use Twig\Environment;
use Twig\Extension\DebugExtension;
use Twig\Loader\FilesystemLoader;

defined('ABSPATH') || die('hard');

/**
 * Класс роутер
 *
 * Подключает Twig шаблонизатор
 *
 * Заменяет стандартные шаблоны на шаблоны twig
 *
 * Отвечает за кеширование
 *
 * @package WSparrow\Modules\Routing
 * @author Chloyka
 * @since 1.0
 */
class WSparrowRouting extends WSparrowRoutingController
{

    /**
     * Содержит инстанс класса
     *
     * @var null|void|static
     * @author Chloyka
     * @since 1.0
     */
    protected static $_instance = null;

    /**
     * Метод инициализации класса
     *
     * Делаем все важные штуки тут
     *
     * @return void
     * @author Chloyka
     * @since 1.0
     */
    public function onInit(): void
    {
        $this->getViewsDirs();
        $base = new FilesystemLoader($this->views_dirs);
        $this->twig = new Environment($base, $this->twig_options);
        $this->twig->addExtension(new DebugExtension());
        add_filter('template_include', array($this, 'checkTemplate'));
        add_filter('template_include', array($this, 'onTemplateInclude'));
    }

    /**
     * Проверяем какой шаблон отобразить
     *
     * @return void
     * @author Chloyka
     * @since 1.0
     */
    public function checkTemplate(): void
    {
        if (is_front_page()) {
            self::$template = 'front-page';
        } elseif (is_post_type_archive()) {
            global $wp_query;
            $post_type = $wp_query->query['post_type'];
            self::$template = "archive-{$post_type}";
        } elseif (is_singular()) {
            global $wp_query;
            $post_type = $wp_query->query['post_type'];
            self::$template = "single-{$post_type}";
        } elseif (is_404()) {
            self::$template = "404";
        }
    }

}
