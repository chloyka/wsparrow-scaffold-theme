<?php


namespace WSparrow\Modules\Routing;

use WP_Query;
use wpdb;
use WSparrow\WSparrowController;

defined('ABSPATH') || die('hard');

/**
 * Класс контекста
 *
 * Отвечает за определение и вывод подключение контекстных данных к шаблонам
 *
 * @package WSparrow\Modules\Routing
 * @author Chloyka
 * @since 1.0
 */
class WSparrowGetRoutingContext extends WSparrowController
{

    /**
     * Содержит инстанс класса
     *
     * @var null|void|static
     * @author Chloyka
     * @since 1.0
     */
    protected static $_instance = null;

    /**
     * Содержит имя файла шаблона
     *
     * @var string
     * @author Chloyka
     * @since 1.0
     */
    private $template;

    /**
     * Инстанс wpdb
     *
     * @var wpdb
     * @author Chloyka
     * @since 1.0
     */
    private $wpdb;

    /**
     * Содержит префикс базы данных
     *
     * @var string
     * @author Chloyka
     * @since 1.0
     */
    private $blog_prefix;


    /**
     * Метод инициализации класса
     *
     * Делаем все важные штуки тут
     *
     * @return void
     * @author Chloyka
     * @since 1.0
     */
    public function onInit(): void
    {
        global $wpdb;
        $this->wpdb = $wpdb;
        $this->blog_prefix = $wpdb->get_blog_prefix();
        $this->template = WSparrowRouting::$template;
    }

    /**
     * Возвращает нужный для рендера страниц контекст
     *
     * @return array
     * @author Chloyka
     * @since 1.0
     */
    public function getContext(): array
    {
        if (is_front_page()) {
            return $this->getFrontPageContext();
        } elseif (is_post_type_archive()) {
            global $wp_query;
            $post_type = $wp_query->query['post_type'];
            return $this->getArchiveContext($post_type);
        } elseif (is_singular()) {
            global $wp_query;
            $post_type = $wp_query->query['post_type'];
            return $this->getSingularContext($post_type);
        } else {
            return array();
        }
    }

    /**
     * Контекст главной страницы
     *
     * @return array
     * @author Chloyka
     * @since 1.0
     */
    private function getFrontPageContext(): array
    {
        $table_name = $this->blog_prefix . "wsparrow_albums";

        $filter = null;

        $additional_sql = null;

        $meta_query = array();

        if (!empty($_GET['sorting'])) {
            $meta_query['relation'] = 'AND';
            $sorting = $_GET['sorting'];

            foreach ($sorting as $key => $item) {
                if ($item != 'false') {
                    if ($key != 'release_year' && $key != 'release_format') {
                        $meta_query[] = array(
                            'key' => '_' . $key . '|||0|value',
                            'value' => $item,
                            'compare' => 'LIKE'
                        );
                    } else {
                        $meta_query[] = array(
                            'key' => '_' . $key,
                            'value' => $item,
                            'compare' => 'LIKE'
                        );
                    }

                    $additional_sql[] = "{$key} LIKE '%{$item}%'";
                }
            }
        }

        $where = is_null($additional_sql) ? '' : "WHERE " . implode(' AND ', $additional_sql);

        $sql = "SELECT post_id, genres, style, release_format, country, release_year FROM {$table_name} {$where}";

        $content = $this->wpdb->get_results($sql, ARRAY_A);

        foreach ($content as $values) {
            $genres = explode(',', $values['genres']);

            foreach ($genres as $genre) {
                $filter['genres'][$genre]['posts'][] = $values['post_id'];
                $filter['genres'][$genre]['count'] = count($filter['genres'][$genre]['posts']);

                if (!empty($_GET['sorting']['genres']) && $_GET['sorting']['genres'] == $genre) {
                    $filter['genres'][$genre]['selected'] = 'selected';
                } else {
                    $filter['genres'][$genre]['selected'] = '';
                }

            }

            $styles = explode(',', $values['style']);

            foreach ($styles as $style) {
                $filter['style'][$style]['posts'][] = $values['post_id'];
                $filter['style'][$style]['count'] = count($filter['style'][$style]['posts']);
                if (!empty($_GET['sorting']['style']) && $_GET['sorting']['style'] == $style) {
                    $filter['style'][$style]['selected'] = 'selected';
                } else {
                    $filter['style'][$style]['selected'] = '';
                }
            }

            $format = $values['release_format'];
            $filter['release_format'][$format]['posts'][] = $values['post_id'];
            $filter['release_format'][$format]['count'] = count($filter['release_format'][$format]['posts']);

            if (!empty($_GET['sorting']['release_format']) && $_GET['sorting']['release_format'] == $format) {
                $filter['release_format'][$format]['selected'] = 'selected';
            } else {
                $filter['release_format'][$format]['selected'] = '';
            }

            $country = $values['country'];
            $filter['country'][$country]['posts'][] = $values['post_id'];
            $filter['country'][$country]['count'] = count($filter['country'][$country]['posts']);

            if (!empty($_GET['sorting']['country']) && $_GET['sorting']['country'] == $country) {
                $filter['country'][$country]['selected'] = 'selected';
            } else {
                $filter['country'][$country]['selected'] = '';
            }

            $year = $values['release_year'];
            $filter['release_year'][$year]['posts'][] = $values['post_id'];
            $filter['release_year'][$year]['count'] = count($filter['release_year'][$year]['posts']);

            if (!empty($_GET['sorting']['release_year']) && $_GET['sorting']['release_year'] == $year) {
                $filter['release_year'][$year]['selected'] = 'selected';
            } else {
                $filter['release_year'][$year]['selected'] = '';
            }
        }

        $posts = null;

        $current_page = !empty($_GET['page_num']) ? $_GET['page_num'] : 1;

        $sort_by = !empty($_GET['sort_by']) ? $_GET['sort_by'] : 'title';

        $order_by = !empty($_GET['order_by']) ? $_GET['order_by'] : 'desc';

        $per_page = !empty($_GET['per_page']) ? $_GET['per_page'] : 18;


        $args = array(
            'post_type' => 'albums',
            'posts_per_page' => $per_page,
            'paged' => $current_page,
            'orderby' => $sort_by,
            'meta_query' => array(
                $meta_query
            ),
            'order' => $order_by,
        );

        $query = new WP_Query($args);

        global $post;

        while ($query->have_posts()) : $query->the_post();
            $single_sql = "SELECT * FROM {$table_name} WHERE `post_id` = {$post->ID}";
            $content = $this->wpdb->get_results($single_sql, ARRAY_A)[0];
            $posts[$post->ID] = $content;
            $posts[$post->ID]['title'] = get_the_title();
            $posts[$post->ID]['url'] = get_permalink();
        endwhile;

        $pagination = paginate_links(array(
            'base' => site_url() . '%_%',
            'format' => '?page_num=%#%',
            'total' => $query->max_num_pages,
            'current' => $current_page,
        ));

        wp_reset_postdata();

        return array(
            'filter' => $filter,
            'posts' => $posts,
            'pagination' => $pagination,
            'sorting' => array(
                'order_by' => $order_by,
                'options' => array(
                    array(
                        'name' => 'По убыванию',
                        'value' => 'desc',
                        'selected' => $order_by == 'desc' ? 'selected' : ''
                    ),
                    array(
                        'name' => 'По возрастанию',
                        'value' => 'asc',
                        'selected' => $order_by == 'asc' ? 'selected' : ''
                    )
                ),
                'per_page' => array(
                    array(
                        'name' => 'Отображать 3',
                        'value' => '3',
                        'selected' => $per_page == '3' ? 'selected' : ''
                    ),
                    array(
                        'name' => 'Отображать 6',
                        'value' => '6',
                        'selected' => $per_page == '6' ? 'selected' : ''
                    ),
                    array(
                        'name' => 'Отображать 12',
                        'value' => '12',
                        'selected' => $per_page == '12' ? 'selected' : ''
                    ),
                    array(
                        'name' => 'Отображать 18',
                        'value' => '18',
                        'selected' => $per_page == '18' ? 'selected' : ''
                    )
                )
            )
        );

    }


    /**
     * Контекст архивных страниц
     *
     * @param $post_type
     * @return array
     * @author Chloyka
     * @since 1.0
     */
    private function getArchiveContext($post_type): array
    {
        $table_name = $this->blog_prefix . "wsparrow_{$post_type}";
        $sql = "SELECT * FROM {$table_name}";
        $result = null;
        $content = $this->wpdb->get_results($sql, ARRAY_A);

        foreach ($content as $key => $value) {
            $result[$key] = $value;
            $result[$key]['url'] = get_permalink($value['post_id']);
            $result[$key]['title'] = get_the_title($value['post_id']);
        }

        return array(
            'context' => $result
        );
    }

    /**
     * Возвращает контекст сингл страниц
     *
     * @param $post_type
     * @return array
     * @author Chloyka
     * @since 1.0
     */
    private function getSingularContext($post_type): array
    {
        global $post;
        $post_id = $post->ID;
        $prefix = $this->blog_prefix . "wsparrow";
        $table_name = "{$prefix}_{$post_type}";
        $sql = "SELECT * FROM `{$table_name}` WHERE `post_id` = {$post_id}";
        $returns = array(
            'title' => get_the_title($post_id),
            'post_id' => $post_id
        );
        $artist = $this->wpdb->get_results($sql, ARRAY_A)[0];
        if ($post_type == 'artists') {
            $albums_sql = "SELECT * FROM `{$prefix}_albums` WHERE `author` = {$post_id}";
            $albums = $this->wpdb->get_results($albums_sql, ARRAY_A);
            $albums_result = null;
            foreach ($albums as $key => $album) {
                $albums_result[$album['release_format']][$key] = $album;
                $albums_result[$album['release_format']][$key]['title'] = get_the_title($album['post_id']);
                $albums_result[$album['release_format']][$key]['url'] = get_the_permalink($album['post_id']);
                $albums_result[$album['release_format']][$key]['tracklist'] = json_decode($album['tracklist']);
            }
            $returns['context']['artist'] = $artist;
            $returns['context']['albums'] = $albums_result;
        } elseif ($post_type == 'albums') {
            $albums_sql = "SELECT * FROM `{$prefix}_albums` WHERE `post_id` = {$post_id}";
            $albums = $this->wpdb->get_results($albums_sql, ARRAY_A)[0];
            $albums['title'] = get_the_title($post_id);
            $albums['author_url'] = get_the_permalink($albums['author']);
            $albums['author'] = get_the_title($albums['author']);
            $albums['url'] = get_the_permalink($post_id);
            $albums['tracklist'] = json_decode($albums['tracklist']);
            $returns['context'] = $albums;
        } else {
            $returns['context'] = $this->wpdb->get_results($sql, ARRAY_A)[0];
        }

        return $returns;
    }

}
