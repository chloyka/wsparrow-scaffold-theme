<?php


namespace WSparrow\Modules\Routing;

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Error\SyntaxError;
use WSparrow\WSparrowController;

abstract class WSparrowRoutingController extends WSparrowController
{
    /**
     * Содержит инстанс класса
     *
     * @var null|void|static
     * @author Chloyka
     * @since 1.0
     */
    protected static $_instance = null;

    /**
     * Содержит настройки Twig
     *
     * @var array
     * @author Chloyka
     * @since 1.0
     */
    protected $twig_options = array(
        'strict_variables' => false,
        'debug' => true,
        'cache' => WSPARROW_CACHE_PATH
    );

    /**
     * Содержит текущее имя шаблона
     *
     * @var string
     * @author Chloyka
     * @since 1.0
     */
    public static $template;

    /**
     * Содержит массив, содержащий структуру папок view
     *
     * @var array
     * @author Chloyka
     * @since 1.0
     */
    protected $views_dirs;

    /**
     * Класс Environment, шаблонизатора twig
     *
     * @var Environment
     * @author Chloyka
     * @since 1.0
     */
    protected $twig;

    /**
     * Рендер функция
     *
     * @param array $context
     * @param null|string $template
     * @author Chloyka
     * @since 1.0
     */
    public function render(array $context, $template = null)
    {
        try {
            if(is_null($template)){
                $template = self::$template;
            }
            $render = $this->twig->render($template . '.twig', $context);
        } catch (LoaderError $e) {
            die($e);
        } catch (RuntimeError $e) {
            die($e);
        } catch (SyntaxError $e) {
            die($e);
        }
        echo $render;
    }


    /**
     * Возвращает все папки с view
     *
     * @param string $directory
     * @param bool $no_dir
     * @author Chloyka
     * @since 1.0
     */
    protected function getViewsDirs($directory = WSPARROW_VIEWS_PATH, $no_dir = false)
    {
        $path = scandir($directory);

        if (false == $no_dir) {
            $this->views_dirs = array($directory);
        }

        foreach ($path as $key => $value) {
            if (!in_array($value, array(".", ".."))) {
                if (is_dir($directory . DIRECTORY_SEPARATOR . $value)) {
                    $this->views_dirs[] = $directory . DIRECTORY_SEPARATOR . $value;
                    $this->getViewsDirs($directory . DIRECTORY_SEPARATOR . $value, true);
                }
            }
        }
    }


    /**
     * Срабатывает на хуке template include
     *
     * Рендерит нужный шаблон
     *
     * @return void
     * @author Chloyka
     * @since 1.0
     */
    public function onTemplateInclude(): void
    {
        $context = WSparrowGetRoutingContext::instance();
        $this->render(
            $context->getContext()
        );
    }
}
