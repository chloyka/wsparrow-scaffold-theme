<?php

namespace WSparrow\Modules\Cpt;

use WSparrow\{Helpers\WSparrowHelpers, WSparrowController};

defined('ABSPATH') || die('hard');

/**
 * Класс для управления Custom Post Types
 *
 * @package WSparrow\Modules\Cpt
 * @author Chloyka
 * @since 1.0
 */
class WSparrowCustomPostTypes extends WSparrowController
{

    /**
     * Содержит инстанс класса
     *
     * @var null|void|static
     * @author Chloyka
     * @since 1.0
     */
    protected static $_instance = null;


    /**
     * Метод инициализации класса
     *
     * Делаем все важные штуки тут
     *
     * @return void
     * @author Chloyka
     * @since 1.0
     */
    public function onInit(): void
    {
        add_filter('wp_insert_post_data', array($this, 'beforeSavePost'), 100, 2);
        $this->registerPostTypes();
    }


    function beforeSavePost($data, $post)
    {
        global $wpdb;
        $table_name = $wpdb->get_blog_prefix() . "wsparrow_{$data['post_type']}";
        $post_id = $post['ID'];

        if ($post_id !== (int)'0' && !empty($post['carbon_fields_compact_input'])) {

            $fields_to_save = $post['carbon_fields_compact_input'];
            $fields_names = $fields_values = $set = array();
            foreach ($fields_to_save as $field => $value) {
                $field = ltrim($field, '_');

                if(is_array($value)){
                    $transient_value = null;
                    if(!empty($value[0]['value'])){
                       foreach ($value as $number => $v){
                           foreach ($v as $key => $val){
                               if($key !== 'value'){
                                   $key = ltrim($key, '_');
                                   $transient_value[$number][$key] = $val;
                               }
                           }
                       }
                        $value = json_encode($transient_value,JSON_UNESCAPED_UNICODE);
                    } else {
                        foreach ($value as $post_ids) {

                            $post_ids = explode(':', $post_ids);
                            $transient_value[] = $post_ids[2];
                        }
                        $value = implode('|', $transient_value);
                    }
                }

                $value = $wpdb->_real_escape(WSparrowHelpers::maybeExplode('|', $value, true));
                $set[] = "`{$field}` = '{$value}'";
                $fields_names[] = "`{$field}`";
                $fields_values[] = "'{$value}'";
            }

            $fields_names = implode(', ', $fields_names);
            $fields_values = implode(', ', $fields_values);

            $set = implode(', ', $set);

            $is_exists = (bool)$wpdb->get_results("SELECT * FROM {$table_name} WHERE `post_id` = {$post_id}");
      
            if (false !== $is_exists) {
                $wpdb->query("UPDATE {$table_name} SET {$set} WHERE `post_id` = {$post_id}");
            } else {
                $wpdb->query("INSERT INTO {$table_name} (`ID`, `post_id`, {$fields_names}) VALUES ('', {$post_id}, $fields_values)");
            }
        }
        return $data;
    }

    /**
     * Метод регистрации типов постов
     *
     * Берет шаблоны для регистрации из json
     *
     * @return void
     * @author Chloyka
     * @since 1.0
     * @link /config/cpt/
     */
    private function registerPostTypes(): void
    {
        $files = WSparrowHelpers::getLocalJsonFileNames('cpt');
        foreach ($files as $file) {
            $template = WSparrowHelpers::getLocalJson($file);
            add_action('init', function () use ($template) {
                register_post_type($template['slug'], $template['settings']);
            });
        }
    }

}
