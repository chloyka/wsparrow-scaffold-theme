<?php


namespace WSparrow\Modules\Cpt;


use WSparrow\WSparrowController;

class WSparrowJSController extends WSparrowController
{

    protected static $_instance = null;

    private $scripts_array = array();

    public function onInit(): void
    {
        $this->filters();
        $this->actions();
    }

    private function filters(): void
    {
        add_filter('script_loader_tag', function ($tag, $handle, $src) {
            return str_replace(' src', ' async defer data-loaded="false" id="' . $handle . '" onload="window.is_loading = false; load_scripts(); this.setAttribute(\'onload\', \'\')" data-src', $tag);
        }, 10, 3);
    }

    private function actions(): void
    {
        add_action('wp_enqueue_scripts', array($this, 'newQueue'), 1);
        add_action('wp_enqueue_scripts', array($this, 'dequeueStyles'));
        add_action('wp_enqueue_scripts', array($this, 'getAllProcessedScripts'));
        add_action('refactoring_scripts', array($this, 'printQueue'));
    }

    /**
     * @return array
     */
    public function parseScriptsNStyles(): array
    {
        global $wp_scripts, $wp_styles;
        $result = array();
        $result['scripts'] = [];
        $result['styles'] = [];

        foreach ($wp_scripts->queue as $script) :
            $result['scripts'][$script] = $wp_scripts->registered[$script]->src;
        endforeach;

        foreach ($wp_styles->queue as $style) :
            $result['styles'][$style] = $wp_styles->registered[$style]->src;
        endforeach;

        return $result;
    }

    public function printQueue()
    {
        $result = '<script>
                            window.is_loading = false;
                            function load_scripts() {
                                setTimeout(function () {
                                        if (window.is_loading === true) {
                                            load_scripts();
                                        } else {
                                            window.is_loading = true;
                                            try{
                                                var script = document.querySelector("script[data-src]");
                                                if(null != script){
                                                    script.src = script.dataset.src;
                                                    script.removeAttribute("data-src");
                                                    script.dataset.loaded = "true";    
                                                }
                                            } catch (e) {
                                              window.is_loading = false;
                                            }
                                        }
                                }, 10);
                            }
                            load_scripts();
                    </script>
                    ';
        echo $result;
    }

    public function dequeueStyles(): void
    {
        $parsed = $this->parseScriptsNStyles();

        foreach ($parsed['styles'] as $k => $v) {
            if ($k != 'admin-bar')
                wp_dequeue_style($k);
        }
    }

    /**
     * @return void
     */
    public function getAllProcessedScripts(): void
    {
        $this->newQueue();

        $parsed = $this->parseScriptsNStyles();

        foreach ($parsed['scripts'] as $k => $v) {
            $this->scripts_array[$k] = $v;
        }

        $this->scripts_array['footer_scripts'] = WSPARROW_JS_URL . 'footer.js';
        if (is_front_page() || is_home()) {
            $this->scripts_array['front_page_scripts'] = WSPARROW_JS_URL . 'front-page.js';
        }

    }

    public function newQueue()
    {

    }
}
