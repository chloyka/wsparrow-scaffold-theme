<?php

namespace WSparrow\Modules\Cpt;

use WSparrow\{WSparrowController};

defined('ABSPATH') || die('hard');

/**
 * Контроллер вывода ассетов на фронт
 *
 * Работает на основе WP фильтров
 *
 * @package WSparrow\Modules\Cpt
 * @author Chloyka
 * @since 1.0
 */
class WSparrowAssetsController extends WSparrowController
{
    /**
     * Содержит инстанс класса
     *
     * @var null|void|static
     * @author Chloyka
     * @since 1.0
     */
    protected static $_instance = null;

    /**
     * Метод инициализации класса
     *
     * Делаем все важные штуки тут
     *
     * @return void
     * @author Chloyka
     * @since 1.0
     */
    public function onInit(): void
    {

    }


    private function renderAssets($file_name)
    {
        $this->getCritical($file_name);
        $this->getMain($file_name);
        $this->getScripts();
    }

    public function enqueueAssets()
    {

    }

    private function getCritical($name)
    {

    }

    private function isLegacy()
    {
        $agent = strtolower($_SERVER['HTTP_USER_AGENT']);
        if (preg_match('/(trident)[\/]([\w.]+)/', $agent))
            return false;
        elseif (preg_match('/(firefox)[\/]([\w.]+)/', $agent))
            return false;
        elseif (preg_match('/(msie)[\/]([\w.]+)/', $agent))
            return false;
        else return true;
    }

    private function getMain($name)
    {

    }

    private function getScripts()
    {

    }

}
