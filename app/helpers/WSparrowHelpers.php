<?php

namespace WSparrow\Helpers;

use wpdb;
use WSparrow\WSparrowInit;

defined('ABSPATH') || die('hard');

/**
 * Вспомогательные функции
 *
 * Класс может содержать только статические функции
 *
 * @package WSparrow\Helpers
 * @author Chloyka
 * @since 1.0
 */
final class WSparrowHelpers
{

    /**
     * Получаем контент из JSON файлов
     *
     * @param string $file
     * @return array
     * @author Chloyka
     * @since 1.0
     */
    public static function getLocalJson(string $file): array
    {
        if (file_exists($file)) {
            return json_decode(
                file_get_contents($file), true
            );
        } else {
            self::makeFilesLastUpdateInfo(WSparrowInit::$slugs_for_checking, true);
            return json_decode(
                file_get_contents($file), true
            );
        }
    }

    /**
     * Получаем массив из имен JSON файлов с указанным слагом
     *
     * @param string $json_slug
     * @return array
     * @author Chloyka
     * @since 1.0
     */
    public static function getLocalJsonFileNames(string $json_slug = ''): array
    {
        if ($json_slug !== '') {
            $json_slug = $json_slug . '/';
        }

        return glob(WSPARROW_CONFIG_PATH . "{$json_slug}*.*");
    }

    /**
     * Генерация хеша для проверки файлов на факт внесения изменений
     *
     * @param array $slugs
     * @param bool $is_current
     * @param bool $is_global_update
     * @return void
     * @author Chloyka
     * @since 1.0
     */
    public static function makeFilesLastUpdateInfo(array $slugs, $is_global_update = false, $is_current = false): void
    {
        $last_text = 'last-updated';
        $current_text = 'current-site-version';
        $is_last = false == $is_current ? $last_text : $current_text;

        foreach ($slugs as $slug) {
            $hash = null;
            $slug = str_replace('.json', '', $slug);
            $files = self::getLocalJsonFileNames($slug);
            foreach ($files as $file) {
                $hash[$file] = filemtime($file);
            }

            if (true === $is_global_update) {
                file_put_contents(WSPARROW_CONFIG_PATH . "last_updated/{$slug}-{$last_text}.json", json_encode($hash));
                file_put_contents(WSPARROW_CONFIG_PATH . "last_updated/{$slug}-{$current_text}.json", json_encode($hash));
            } else {
                $hash_path = WSPARROW_CONFIG_PATH . "last_updated/{$slug}-{$is_last}.json";
                file_put_contents($hash_path, json_encode($hash));
            }

        }
    }

    /**
     * Делаем тестовый запрос к базе для проверки существования таблицы
     *
     * @param wpdb $wpdb
     * @param string $table_name
     * @return bool
     * @author Chloyka
     * @since 1.0
     */
    public static function testQuery(wpdb $wpdb, string $table_name): bool
    {
        $table = $wpdb->get_blog_prefix() . "wsparrow_{$table_name}";
        $wpdb->hide_errors();
        $is_exists = (bool)$wpdb->get_results("SELECT 1 from {$table}", ARRAY_A);
        if (isset($is_exists) && false !== $is_exists) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Формирует массив из строки, если разделитель существует
     *
     * Возвращает json строку, если требуется
     * json строка возвращается только в случае, если строка была преобразованна в массив
     *
     * Возвращает исходную строку, если разделитель отсутствует
     *
     * @param string $delimiter
     * @param string $haystack
     * @param bool $return_json
     * @return string
     * @author Chloyka
     * @since 1.0
     */
    public static function maybeExplode(string $delimiter, string $haystack, bool $return_json = false): string
    {
        if (false !== strpos($haystack, $delimiter)) {
            $haystack = explode($delimiter, $haystack);
            if (false !== $return_json) {
                $haystack = json_encode($haystack, JSON_UNESCAPED_UNICODE);
            }
        }
        return $haystack;
    }

}
