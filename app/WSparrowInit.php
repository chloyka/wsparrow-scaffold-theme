<?php

namespace WSparrow;

use WSparrow\{Modules\Cpt\WSparrowCustomPostTypes,
    Modules\CustomFields\WSparrowCustomFields,
    Modules\Migrations\WSparrowMigrations,
    Helpers\WSparrowHelpers,
    Modules\Routing\WSparrowRouting,
    Modules\Rest\WSparrowCarouselRest};
use Twig\Environment;
use Twig\Loader\FilesystemLoader;

defined('ABSPATH') || die('hard');

/**
 * Инициализирующий класс
 *
 * Собирает приложение из модулей
 *
 * Модули необходимо декларировать и вызывать по аналогии
 *
 * @package WSparrow
 * @author Chloyka
 * @version 1.0
 */
class WSparrowInit extends WSparrowController
{

    /**
     * Содержит инстанс класса
     *
     * @var null|void|static
     * @author Chloyka
     * @since 1.0
     */
    protected static $_instance = null;

    /**
     * Содержит инстанс WSparrowRouting
     *
     * @var WSparrowRouting
     * @author Chloyka
     * @since 1.0
     */
    private $routing;

    /**
     * Содержит слаги файлов конфигураций, которые требуется проверять на предмет изменений
     *
     * @var array
     */
    public static $slugs_for_checking = array(
        'migrations', 'custom-fields'
    );

    /**
     * Содержит инстанс WSparrowCustomPostTypes
     *
     * @var WSparrowCustomPostTypes
     * @author Chloyka
     * @since 1.0
     */
    private $cpt;

    /**
     * Содержит инстанс WSparrowMigrations
     *
     * @var WSparrowMigrations
     * @author Chloyka
     * @since 1.0
     */
    private $migrations;

    /**
     * Содержит инстанс WSparrowCustomFields
     *
     * @var WSparrowCustomFields
     * @author Chloyka
     * @since 1.0
     */
    private $custom_fields;


    /**
     * @var WSparrowCarouselRest
     */
    private $carousel;

    /**
     * Метод инициализации класса
     *
     * Делаем все важные штуки тут
     *
     * @return void
     * @author Chloyka
     * @since 1.0
     */
    public function onInit(): void
    {
        if(is_admin()){
            WSparrowHelpers::makeFilesLastUpdateInfo(self::$slugs_for_checking);
        }
        $this->registerModules();
    }

    /**
     * Регистрируем модули темы
     *
     * @return void
     * @author Chloyka
     * @since 1.0
     */
    private function registerModules(): void
    {
        $this->cpt = WSparrowCustomPostTypes::instance();
        $this->migrations = WSparrowMigrations::instance();
        $this->custom_fields = WSparrowCustomFields::instance();
        $this->routing = WSparrowRouting::instance();
        $this->carousel = WSparrowCarouselRest::instance();
    }


}
