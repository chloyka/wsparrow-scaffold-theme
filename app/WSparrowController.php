<?php

namespace WSparrow;

use Exception;

defined('ABSPATH') || die('hard');

/**
 * Class WSparrowController
 *
 * Главный абстрактный класс приложения
 *
 * Задает структуру контроллеров для всего приложения
 *
 * @package WSparrow\Modules
 * @author Chloyka
 * @since 1.0
 */
abstract class WSparrowController implements IWSparrowController
{

    /**
     * Содержит инстанс класса
     *
     * @var null|void|static
     * @author Chloyka
     * @since 1.0
     */
    protected static $_instance = null;

    /**
     * Конструктор контроллера
     *
     * Вызывает метод onInit;
     *
     * @author Chloyka
     * @since 1.0
     */
    protected function __construct(){
        $this->onInit();
    }

    /**
     * Метод инициализации класса
     *
     * Делаем все важные штуки тут
     *
     * @return void
     * @author Chloyka
     * @since 1.0
     */
    abstract public function onInit(): void;

    /**
     * Геттер
     *
     * @param string $property
     * @return mixed
     * @throws Exception;
     * @author Chloyka
     * @since 1.0
     */
    public function __get(string $property)
    {
        if (property_exists($this, $property)) {
            return $this->$property;
        } else {
            throw new Exception('Неизвестное свойство');
        }
    }

    /**
     * Сеттер
     *
     * @param string $property
     * @param string|array $value
     * @return void
     * @throws Exception
     * @author Chloyka
     * @since 1.0
     */
    public function __set(string $property, $value): void
    {
        if (property_exists($this, $property)) {
            $this->$property = $value;
        } else {
            throw new Exception('Неизвестное свойство');
        }
    }

    /**
     * Возвращаем метод класса
     *
     * @param string $method
     * @param null|array $args
     * @return callable
     * @throws Exception
     * @author Chloyka
     * @since 1.0
     */
    public function getMethod(string $method, array $args = null)
    {
        if (method_exists($this, $method)) {
            return call_user_func_array(array($this, $method), $args);
        } else {
            throw new Exception('Неизвестный метод');
        }

    }

    /**
     * Возвращает текущий экземпляр объекта
     *
     * @return void|static|null
     * @author Chloyka
     * @since 1.0
     */
    public static function instance()
    {

        if (isset($_POST['action']) && $_POST['action'] === 'heartbeat') {
            return;
        }

        if (is_null(static::$_instance)) {
            static::$_instance = new static();
        }

        return static::$_instance;

    }
}
