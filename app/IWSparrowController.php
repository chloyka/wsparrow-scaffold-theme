<?php

namespace WSparrow;

use Exception;

defined('ABSPATH') || die('hard');

/**
 * Interface IWSparrowController
 *
 * @package WSparrow
 * @author Chloyka
 * @since 1.0
 */
interface IWSparrowController
{

    /**
     * Метод инициализации класса
     *
     * Делаем все важные штуки тут
     *
     * @return void
     * @author Chloyka
     * @since 1.0
     */
    public function onInit(): void;

    /**
     * Геттер
     *
     * @param string $property
     * @return mixed
     * @throws Exception;
     * @author Chloyka
     * @since 1.0
     */
    public function __get(string $property);

    /**
     * Сеттер
     *
     * @param string $property
     * @param string|array $value
     * @author Chloyka
     * @since 1.0
     */
    public function __set(string $property, $value);

    /**
     * Возвращаем метод класса
     *
     * @param string $method
     * @param null|array $args
     * @return callable
     * @throws Exception
     * @since 1.0
     * @author Chloyka
     */
    public function getMethod(string $method, array $args = null);


    /**
     * Возвращает текущий экземпляр объекта
     *
     * @return void|static|null
     * @author Chloyka
     * @since 1.0
     */
    public static function instance();

}
