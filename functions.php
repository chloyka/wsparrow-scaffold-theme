<?php
/**
 * functions.php
 *
 * Писать функционал тут - запрещено
 *
 * Файл должен оставаться без изменений
 *
 * Весь функционал должен прописываться через модули.
 *
 * @package WSparrow
 * @author Chloyka
 * @since 1.0
 */

/*
|--------------------------------------------------------------------------
| Подключаем Composer autoload
|--------------------------------------------------------------------------
|
| Все файлы внутри папки app будут включены в autoloader
| В случае, если подключение вызывает ошибку необходимо обновить autoloader с помощью команды composer dump-autoload -o
|
*/
require_once 'vendor/autoload.php';

use WSparrow\WSparrowInit;

defined('ABSPATH') || die('hard');

/**
 * Режим продакшена
 *
 * При false все автоматические действия, способные навредить работе сайта:
 * Добавление миграций, обновление таблиц, удаление неиспользованных столбцов в бд итд
 * Будут происходить в автоматическом режиме.
 *
 * В противном случае все действия должны проводиться через wscli ли при помощи REST API (планируется добавить управление в админ панель)
 *
 * @author Chloyka
 * @since 1.0
 */
defined('WSPARROW_PRODUCTION_MODE') || define('WSPARROW_PRODUCTION_MODE', false);

/**
 * Абсолютный путь к теме
 *
 * @author Chloyka
 * @since 1.0
 */
defined('WSPARROW_PATH') || define('WSPARROW_PATH', __DIR__ . '/');

/**
 * Абсолютный путь к файлам кеша
 *
 * @author Chloyka
 * @since 1.0
 */
defined('WSPARROW_CACHE_PATH') || define('WSPARROW_CACHE_PATH', __DIR__ . '/resources/cache/');

/**
 * Абсолютный путь к файлам шаблонов
 *
 * @author Chloyka
 * @since 1.0
 */
defined('WSPARROW_VIEWS_PATH') || define('WSPARROW_VIEWS_PATH', __DIR__ . '/resources/views/');

/**
 * Абсолютный путь к директории конфигураций
 *
 * @author Chloyka
 * @since 1.0
 */
defined('WSPARROW_CONFIG_PATH') || define('WSPARROW_CONFIG_PATH', __DIR__ . '/config/');

/*
|--------------------------------------------------------------------------
| Проверяем, делается ли CLI запрос
|--------------------------------------------------------------------------
*/
if(!defined('WSPARROW_DO_CLI')) {
    /**
     * Абсолютный путь к файлам CSS
     *
     * @author Chloyka
     * @since 1.0
     */
    defined('WSPARROW_CSS_PATH') || define('WSPARROW_CSS_PATH', __DIR__ . '/public/assets/css');

    /**
     * URL путь к файлам CSS
     *
     * @author Chloyka
     * @since 1.0
     */
    defined('WSPARROW_CSS_URL') || define('WSPARROW_CSS_URL', get_template_directory_uri() . '/public/assets/css');

    /**
     * Абсолютный путь к файлам JS
     *
     * @author Chloyka
     * @since 1.0
     */
    defined('WSPARROW_JS_PATH') || define('WSPARROW_JS_PATH', __DIR__ . '/public/assets/js');

    /**
     * URL путь к файлам JS
     *
     * @author Chloyka
     * @since 1.0
     */
    defined('WSPARROW_JS_URL') || define('WSPARROW_JS_URL', get_template_directory_uri() . '/public/assets/js');
}

/*
|--------------------------------------------------------------------------
| Проверяем, не объявлена ли функция в дочерней теме
|--------------------------------------------------------------------------
*/
if (!function_exists('wSparrowInitApp')) {
    /**
     * Функция вызова инициализирующего класса
     *
     * @return void|WSparrowInit|null
     * @author Chloyka
     * @since 1.0
     */
    function wSparrowInitApp()
    {
        return WSparrowInit::instance();
    }

    defined('WSPARROW_DO_CLI') || wSparrowInitApp();
}
